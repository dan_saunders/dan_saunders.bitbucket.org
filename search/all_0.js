var searchData=
[
  ['addchannels',['addchannels',['../class_s_trigger_record.html#aa18a33ed61b87db2fc233a0917592bd2',1,'STriggerRecord']]],
  ['addinputdatafilenames',['addInputDataFileNames',['../class_s_clipboard.html#a571b4a71b82677eb9553f7f6544c06b7',1,'SClipboard']]],
  ['addmessage',['addMessage',['../class_s_run_status.html#a06950100b7a7e335999a5bfe65e83192',1,'SRunStatus']]],
  ['addpeak',['addPeak',['../class_s_event.html#aeeba40f95c656e373c432244f15f009b',1,'SEvent']]],
  ['addtimeblock',['addTimeBlock',['../class_s_clipboard.html#af9ab8a74bb9e0bc9f63d6b06c5ece446',1,'SClipboard']]],
  ['addtriggerrecord',['addTriggerRecord',['../class_s_time_block.html#aa1b9999f04a0e73edf648b8ce2d58c96',1,'STimeBlock']]],
  ['addwaveform',['addWaveform',['../class_s_channel.html#a4cd734fc5e9eff0b737dfd388ea9ad05',1,'SChannel']]],
  ['addwaveformblock',['addWaveformBlock',['../class_s_time_block.html#a8234b8ea9ff525d74bddbdf0d842fffe',1,'STimeBlock']]],
  ['algoheaders_2eh',['algoHeaders.h',['../algo_headers_8h.html',1,'']]],
  ['algos',['algos',['../class_s_clipboard.html#a8e23896fe5eaacbd3cc507e3cc4c99ae',1,'SClipboard']]],
  ['amplitude',['amplitude',['../class_s_peak.html#a83e94617753ae998a2ceceea383d8022',1,'SPeak']]],
  ['appendalgorithm',['appendAlgorithm',['../class_s_saffron.html#afc659ae92401973eaea6fd2ba04c084a',1,'SSaffron']]]
];
