var searchData=
[
  ['name',['name',['../class_i_s_option.html#adbc16a35696b53802a28450dfedaa894',1,'ISOption::name()'],['../class_i_s_algorithm.html#a7278896d6ac697c7beb351b57452b7ee',1,'ISAlgorithm::name()']]],
  ['nchannels',['nChannels',['../class_s_detector.html#ad4002fd11e9039d61503444a9c33c383',1,'SDetector']]],
  ['ncyclelimit',['nCycleLimit',['../class_s_saffron.html#a4ac175d38840a603ebe889617e1cc94b',1,'SSaffron']]],
  ['nextblock',['nextblock',['../class_s_raw_buffer.html#abd4d6b332590ce05a93777eb61f46ffc',1,'SRawBuffer']]],
  ['nid_5fiona',['NID_IonA',['../class_s_event.html#a07f9734f9df0b497942011554a1e9e5f',1,'SEvent']]],
  ['nid_5fnpeaks',['NID_nPeaks',['../class_s_event.html#a37dbeb83091fdafad6c9637e2deeb7bc',1,'SEvent']]],
  ['nid_5ftot',['NID_tot',['../class_s_event.html#a087334af6e4824b6384a9b6129b43cba',1,'SEvent']]]
];
