var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstvwxyz~",
  1: "is",
  2: "acims",
  3: "abcdefghilmnoprstwxyz~",
  4: "bcdghilmrtvw",
  5: "bips",
  6: "eipstw",
  7: "b",
  8: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Pages"
};

